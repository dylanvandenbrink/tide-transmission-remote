// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace TransmissionRemote.ViewControllers
{
    [Register ("TorrentsTableViewController")]
    partial class TorrentsTableViewController
    {
        [Action ("OnTestButtonClick:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void OnTestButtonClick (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
        }
    }
}