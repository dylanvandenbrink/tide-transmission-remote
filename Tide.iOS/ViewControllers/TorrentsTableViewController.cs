﻿using System;

using UIKit;

namespace TransmissionRemote.ViewControllers
{
    public partial class TorrentsTableViewController : UITableViewController
    {
        public TorrentsTableViewController(IntPtr handle) : base(handle) { }
        public TorrentsTableViewController() : base("TorrentsTableViewController", null) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        partial void OnTestButtonClick(UIBarButtonItem sender)
        {
            
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

