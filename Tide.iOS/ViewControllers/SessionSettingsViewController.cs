﻿using System;

using UIKit;

namespace TransmissionRemote.ViewControllers
{
    public partial class SessionSettingsViewController : UITableViewController
    {
        public SessionSettingsViewController(IntPtr handle) : base(handle) { }
        public SessionSettingsViewController() : base("SessionSettingsViewController", null) { }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        partial void OnCloseButtonClick(UIBarButtonItem sender)
        {
            this.DismissViewController(true, null);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

