# Tide - Transmission Remote #

A [Transmission](https://transmissionbt.com) remote client for iOS created in C# with Xamarin.

### Who do I talk to? ###

* Dylan van den Brink - [Telegram](https://t.me/dylanvdbrink)

### How can I help? ###

* Contribute by fixing bugs you may find and creating a pull request
* Buy me a beer - [Paypal](paypal.me/dylanvdbrink)